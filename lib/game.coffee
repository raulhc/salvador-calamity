{log, toJSON} = require './helpers'
maps = require './maps-api'
updater = require './clientUpdater'
MapObject = require './map-object'

game = module.exports

updateObjects = ->
  clientUpdate = objects: {}
  for name,obj of game.objects
    # Update positions
    do obj.move
    # Select data to the client
    clientUpdate.objects[name] =
      name: name
      lat: obj.lat
      lng: obj.lng
      destin: obj.destin
      step: if obj.steps? then obj.steps[0] else null
      pic: switch name
        when /^police/ then 'images/icons/police-#pct#.png'
        else obj.pic
  updater.broadcast clientUpdate

game.init = ->
  game.objects = {}
  game.events = {}
  new MapObject game, 'home',
    lat: -13.001925, lng: -38.52942, pic: 'images/icons/home-#pct#.png'
  clearInterval game.interval if game.interval?
  game.interval = setInterval updateObjects, 5000
  gamaInitAddr = 'Centro Administrativo da Bahia, Salvador-BA, Brasil'
  maps.getLatLng gamaInitAddr, (err, ll)->
    try
      if err
        log 'getLatLng for CAB FAIL', err
        return setTimeout game.init, 5000
      gama = new MapObject game, 'gama', lat:ll.lat, lng:ll.lng
      gama.pic = 'images/icons/hackergotchi-gama-#pct#.png'
      gama.onReachesDestination = -> @setRandonDestination 0.01
      gama.setRandonDestination 0.02
    catch err
      log 'Gama initialization Fail', err
      return setTimeout game.init, 5000
  neighbohoods = [
    'Barra', 'Vitória', 'Brotas', 'Pituba', 'Pituaçu', 'Piraja', 'São Marcos',
    'Cajazeiras', 'Itapuã', 'São Cristóvão', 'Tancredo Neves'
  ]
  for neighbohood,index in neighbohoods
    do (neighbohood)->
      policePlace = neighbohood + ', Salvador-BA, Brasil'
      setTimeout (-> # delay it to prevent API block
        maps.getLatLng policePlace, (err, ll)->
          try
            return log "getLatLng for #{neighbohood} FAIL", err if err
            police = new MapObject game, 'police '+neighbohood,
                                   lat:ll.lat, lng:ll.lng
            police.pic = 'images/icons/police-#pct#.png'
            police.onReachesDestination = -> @setRandonDestination 0.01
            police.setRandonDestination 0.01
          catch err
            log 'Gama initialization Fail', err
            return setTimeout game.init, 5000
      ), index*1000

