configs = require '../config'
event = require './event'
{log} = require './helpers'
maps = require './maps-api'

availableActions = -> 
  eventClasses = [event.Fire, event.Cops, event.Shooting] # TODO Add every type
  actions = []
  for klass in eventClasses
    actions = actions.concat klass.availableActions
  actions

pattern = ->
  p = []
  for action in do availableActions
    p.push('('+action+')')
  p = p.join('|')
  new RegExp(p, "i");

parseAction = (text) ->
  try
    text.match(do pattern)[0]
  catch
    null

parseAddress = (text) ->
  #text.replace(do pattern, '').replace(/\#ssa/i, 'Salvador-BA')
  text.replace(do pattern, '').replace(/\#fake_ssa/i, 'Salvador-BA')

class Action
  constructor: (text, @lat, @lng) ->
    @verb = parseAction text
    return if !@verb
    @address = parseAddress text
    @lat = parseFloat @lat.toString().replace(/(\.[0-9]{3}).*/, '$1')
    @lng = parseFloat @lng.toString().replace(/(\.[0-9]{3}).*/, '$1')

  toId: ->
    @lat + ',' + @lng + '-' + do @toEvent

  toEvent: ->
    switch @verb
      when 'incendiar' then 'Fire'
      when 'apagar incendio' then 'Fire'
      when 'chamar policia' then 'Cops'
      when 'fazer um tiroteio' then 'Shooting'
      else null

  valid: ->
    @verb?

  setEvent: (e) ->
    @event = e

  setUser: (u) ->
    @user = u

  disengage: ->
    @event.removeMember()

exports.actionBuilder =  (text, callback) ->
  maps.getLatLng parseAddress(text), (err, ll) ->
    return callback(err) if err
    a = new Action text, ll.lat, ll.lng
    callback null, a
