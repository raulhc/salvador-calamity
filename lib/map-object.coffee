{log, toJSON} = require './helpers'
maps = require './maps-api'

randWay = -> if Math.random() < 0.5 then -1 else 1

class MapObject

  constructor: (@game, @name, {@lat, @lng, @pic}=config)->
    @game.objects[@name] = this

  setDestination: (dLat, dLng)->
    @destin = { lat:dLat, lng:dLng }
    maps.getDirections orig:[@lat, @lng], dest:[dLat, dLng],
      (err, data)=>
        if err
          @destin = null
        else
          @steps = data.routes[0].legs[0].steps

  setRandonDestination: (inc=0.01)->
    @setDestination @lat+inc*randWay(), @lng+inc*randWay()

  move: ->
    if @steps? and @steps[0]?
      duration = @steps[0].duration
      duration.currValue ||= duration.value + 1
      duration.currValue -= 20
      duration.currValue = 0 if duration.currValue < 0
      do @endStep if duration.currValue is 0

  endStep: ->
    lastStep = do @steps.shift
    @lat = lastStep.end_location.lat
    @lng = lastStep.end_location.lng
    if @steps.length is 0
      @steps = null
      @destin = null
      do @onReachesDestination if @onReachesDestination?
    else
      @steps[0].duration.currValue = @steps[0].duration.value

module.exports = MapObject
