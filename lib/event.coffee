configs = require '../config'
{log} = require './helpers'
game = require './game'
   
events = exports.events = {}

exports.findOrCreate = (action) ->
  eventId = do action.toId
  events[eventId] ||= new exports[do action.toEvent] action

class exports.Event
  # Subclasses must define it
  @availableActions: []

  constructor: (action) ->
    @lat = action.lat
    @lng = action.lng
    @members = {}

  actionsCount: ->
    counts = {}
    for userId, verb of @members
      counts[verb] ||= 0
      counts[verb]++
    counts

  addMember: (userId, action) ->
    @members[userId] = action.verb 
    do @update

  removeMember: (userId) ->
    if @members[userId]?
      delete @members[userId]
      do @update

  verify: ->
    # Subclasses must define this method
    # Should return status of the event
    
  update: ->
    @push @verify()

  push: (status) ->
    game.events.push {id: @constructor.name, lat: @lat, lng: @lng, status: status}

# Concrete events

exports.Fire = require './events/fire'
exports.Cops = require './events/cops'
exports.Shooting = require './events/shooting'
