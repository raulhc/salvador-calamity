event = require '../event'
{log} = require '../helpers'

class Fire extends event.Event

  # TODO review this value
  membersNeeded: 2

  @availableActions: ['incendiar', 'apagar incendio']

  verify: ->
    counts = do @actionsCount
    putFire = counts['incendiar'] || 0
    removeFire = counts['apagar incendio'] || 0
    result = putFire - removeFire
    log "Verificando possibilidade de incêndio..."

    if result >= @membersNeeded
      'complete'
    else if result > 0 and result < @membersNeeded
      'incomplete'
    else
      'over'

module.exports = Fire
