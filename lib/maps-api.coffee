{log} = require './helpers'
http = require 'http'

# Get json response from Google maps API
getJSON = (path, callback)->
  options =
    hostname: 'maps.googleapis.com',
    port: 80,
    path: "/maps/api/#{path}&sensor=false"
  req = http.request options, (res)->
    bodyData = ''
    res.on 'data', (chunk)->
      bodyData += chunk.toString()
    res.on 'end', ->
      try
        json = JSON.parse bodyData
      catch err
        return callback err, bodyData
      callback null, json
  req.on 'error', (err)-> callback err
  do req.end

exports.getDirections = ({orig,dest,mode}=options, callback)->
  getJSON "directions/json?" +
          "origin=#{orig[0]},#{orig[1]}&" +
          "destination=#{dest[0]},#{dest[1]}&" +
          "mode=#{mode||'walking'}", callback

exports.getLatLng = (address, callback)->
  getJSON "geocode/json?address=#{encodeURIComponent address}", (err, data)->
    if err
      callback err, data
    else
      callback null, data.results[0].geometry.location

