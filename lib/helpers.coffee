{inspect} = require 'util'

exports.log = (args...)->
  date = (new Date).toISOString().replace /(.*)T(.*)\..*/,'$1 $2'
  data = []
  for a in args
    data.push if typeof a is 'string' then a else inspect a
  console.log date + ' ➡ ' + data.join ' '

preventCircular = (obj)->
  cache = [];
  (key, value)->
    if value? and typeof value is 'object'
      return '[Circular]' if value in cache
      cache.push value
    value

exports.toJSON = (obj)-> JSON.stringify obj, preventCircular(obj), '  '

