{log, toJSON} = require './helpers'

io = null

exports.init = (server)->
  io = require('socket.io') server
  io.on 'connection', (socket)->
    log 'User in!', socket.handshake.address, socket.handshake.headers['user-agent']
    socket.on 'event', (data)-> log 'EVENT', data
    socket.on 'disconnect', -> 'User Out'

exports.broadcast = (data)->
  io.emit 'update', data

