Twitter = require 'twitter'
configs = require '../config'
action = require './action'
event = require './event'
user = require './user'
{log} = require './helpers'

class exports.Observer
  constructor: ->
    @client = new Twitter({
      consumer_key: configs.twitter.ConsumerKey
      consumer_secret: configs.twitter.ConsumerSecret
      access_token_key: configs.twitter.AccessToken
      access_token_secret: configs.twitter.AccessTokenSecret
    })
    @users = {}
    @actions = {}
    @actions_members = {}

  registerUser: (userData) ->
    @users[userData.screen_name] = new user.User userData

  registerAction: (text, userData) ->
    # Parse action
    action.actionBuilder text, (err, a) =>
      return log(err) if err

      return unless a.valid()
      log 'Action: ', a

      # Register user informations
      @registerUser userData
      u = @users[userData.screen_name]
      log 'Register user: ', u.data.name

      # Remove previous action register to change user action
      userEvent = do u.getEvent
      if userEvent?
        log 'Removing previous event: ', userEvent
        userEvent.removeMember u.id

      # Register user on event
      e = event.findOrCreate a
      e.addMember u.id, a
      log 'Register event: ', e
 
  watch: (pattern) ->
    @client.stream 'filter', {track: pattern}, (stream) =>
      stream.on 'data', (tweet) =>
        @registerAction tweet.text, tweet.user
        #log tweet

      stream.on 'error', (error) ->
        log error
