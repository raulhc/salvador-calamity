configs = require '../config'
event = require './event'
{log} = require './helpers'

users = {}

class exports.User
  constructor: (data) ->
    @id = data.screen_name
    @data = data
    @position = null

  getEvent: ->
    for eId, e of event.events
      for uId of e.members
        return e if uId is @id
        
