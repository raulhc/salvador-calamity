fs = require 'fs'
http = require 'http'
maps = require './lib/maps-api'
twitter = require './lib/twitter'
{log, toJSON} = require './lib/helpers'

server = http.createServer (req, res)->
  file = decodeURIComponent(req.url[1..]).replace /\.+/, '.'
  file = 'index.html' if file is ''
  ext = file.replace /^.*\.([^.]+)$/, '$1'
  contentType = 'text/plain'
  switch ext
    when 'js' then contentType = 'text/javascript'
    when 'css' then contentType = 'text/css'
    when 'html' then contentType = 'text/html'
    when 'png' then contentType = 'image/png'
  fs.readFile file, (err, data)->
    if err
      res.writeHead 200, 'Content-Type': 'text/plain'
      res.end "Fail to read the file #{file}:\n\n#{toJSON err}"
    else
      res.writeHead 200, 'Content-Type': contentType
      res.end data

server.listen 3000

# Server push:
require('./lib/clientUpdater').init server

game = require './lib/game'
setTimeout game.init, 1000

observer = new twitter.Observer
observer.watch '#ssa'
