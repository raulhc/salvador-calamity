# Salvador Calamity

## Explanation

* __What do we do now Salvador?__

A game created to the Global Game Jam 2015 by Raul Hacker Club.

## About the game

* __What happened?__

A weird explosion in CAB and public opinio believes that Mr. Gama are guilty for this.

* __What the objective of game?__

Every player can help or hinder Mr. Gama arrive at home.

* __Why home and not a real safe place?__

Because Mr. Gama may prove his innocence arriving home, or not.

* __How can we play?__

Sending Twitter commands! Look how it is cool and easy too. 

* __Who is Mr. Gama?__

He is a nice robot. Please help him to arrive safe.

* __This is a multiplayer game?__

YEAH! \o/

* __What is the possible actions to interact?__

Blocking the street (Bloquear a rua)

Parede/Disguise Passeata/disfarce

Give Water (Dar água)

Give food (Dar comida)

Call family blackmail (Telefonema Familiares Chantagem)

Parede "Against or Helping" (Manifestação contra ou a favor)

Fear (Medo)

Support posts (Apoio mensagens)

Denounce police (Denunciar a policia)

Landslip (Desabamento)

Flooding (Alagamento)

Zumbi Apocalypse (Apocalipse Zumbi)


## Installing

Read `INSTALL.md`
