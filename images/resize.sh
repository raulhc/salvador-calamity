#!/bin/bash

cd "$(dirname "$0")"
mkdir icons

for pct in $(seq 20 10 100); do
  convert police.png -resize $(($pct/5))% icons/police-$pct.png
  convert hackergotchi-gama.png -resize $pct% icons/hackergotchi-gama-$pct.png
  inkscape -d $(( 90+($pct-20) )) -e icons/home-$pct.png home.svg
done
