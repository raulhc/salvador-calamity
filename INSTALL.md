# Installing Salvador Calamity

Simply run this commands:
```bash
git clone https://gitlab.com/raulhc/salvador-calamity.git
cd salvador-calamity
npm install # will install the app dependences in this directory
images/resize.sh # create and update icons
```

Than you can run it!
```bash
npm start
```

The app is up and running on your machine at `http://localhost:3000`

If you want it full running receiving twitter commands, copy `config.json.dist` to `config.json` and fill it with your app keys.

## Troubleshooting

### I don't have the git command

If you are using GNU/Linux, run `apt-get install git`.
If you are not using GNU/Linux, I don't know why are you paying for something that can't solve your needs simpler than a free *(as in freedom)* system.

### I don't have the npm command

If you are using GNU/Linux, run `apt-get install nodejs npm`.
If you are not using GNU/Linux, you have a problem.

