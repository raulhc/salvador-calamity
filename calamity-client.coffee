map = null # will be defined by `initialize()` method.

mapOptions =
  center: { lat: -12.95, lng: -38.4 }
  zoom: 12
map = new google.maps.Map document.getElementById('map'), mapOptions

geo = new google.maps.Geocoder

addressIsSSA = (address_components)->
  ok = true
  for component in address_components
    ok = false if component.types[0] is 'administrative_area_level_2' and
                  component.short_name isnt 'Salvador'
    ok = false if component.types[0] is 'administrative_area_level_1' and
                  component.short_name isnt 'BA'
    ok = false if component.types[0] is 'country' and
                  component.short_name isnt 'BR'
  return ok

google.maps.event.addListener map, 'click', (event)->
  geo.geocode location: event.latLng, (results, status)->
    return console.log 'geocode fail', status unless status is 'OK'
    address = []
    for component in results[0].address_components
      break if component.long_name is 'Salvador'
      if component.types[0] is 'street_number'
        address.push 'nº' + component.short_name.split('-')[0]
      else
        address.push component.short_name
    unless addressIsSSA results[0].address_components
      alert 'The game is only in Salvador - Bahia - Brazil'
    else # Ok, the user click in Salvador
      console.log address
      showTweetDialog address

mkRadio = (name, value, label)->
  $('<label><input type="radio" name="' + name + '"' +
    ' value="' + value + '">' + label + '</label>')

showTweetDialog = (address)->
  dialog = $('<div title="What do we do now?">')
  $("<p>Você clicou em #{address.join(', ')}</p>").appendTo dialog
  $("<p>O que você vai iniciar ou participar?</p>").appendTo dialog
  mkRadio('to-do', 'incendiar', 'Incendiar').appendTo dialog
  mkRadio('to-do', 'apagar o incendio', 'Apagar Incendio').appendTo dialog
  mkRadio('to-do', 'chamar a polícia', 'Chamar a Polícia').appendTo dialog
  mkRadio('to-do', 'fazer um tiroteio', 'Fazer um Tiroteio').appendTo dialog
  dialog.dialog modal: true, width: 800
  countClick = 0
  $('input', dialog).on 'click', ->
    if ++countClick is 1
      $('<hr>').appendTo dialog
      $('<a id="twett-it" href="#" target="_blank">Faça isso!</a>').appendTo dialog
      $('<p>As ações deste jogo acontecem por meio do Twetter.' +
        ' Inclusive, você nem precisa vir aqui para fazer uma ação,' +
        ' basta twittar.</p>').appendTo dialog
    setTimeout (=> updateTweet @value, address), 100

updateTweet = (action, address)->
  address = address.concat '#SSA'
  url = 'https://twitter.com/intent/tweet' +
  '?original_referer=' + encodeURIComponent(document.location.href) +
  '&text=' + encodeURIComponent("Vou #{action} em #{address.join(', ')}.")
  $('#twett-it').attr('href', url)

socket = do io

mapObjects = {}
addMapObject = (gameObjData)->
  latlng = new google.maps.LatLng gameObjData.lat, gameObjData.lng
  title = if gameObjData.name is 'gama' then 'Mr. Gama' else gameObjData.name
  marker = new google.maps.Marker position: latlng, map: map, title: title
  mapObjects[gameObjData.name] = marker
  marker.gameData = gameObjData
  calcIconZoom marker

calcIconZoom = (marker)->
  if typeof marker.gameData.pic is 'string'
    zoom = map.getZoom()
    pct = 90 - (20-zoom) * 10
    pct = 100 if zoom > 20
    pct = 20 if zoom < 13
    marker.setIcon marker.gameData.pic.replace /#pct#/, pct

google.maps.event.addListener map, 'zoom_changed', ->
  calcIconZoom obj for name,obj of mapObjects

socket.on 'update', (data)->
  for name, gameObjData of data.objects
    if mapObjects[name]?
      mapObjects[name].gameData = gameObjData
    else
      addMapObject gameObjData
    animateMarkerMove mapObjects[name]

animateMarkerMove = (marker)->
  if step = marker.gameData.step
    time = step.duration.value
    currValue = step.duration.currValue
    points = google.maps.geometry.encoding.decodePath(step.polyline.points)
    index = Math.round (points.length - 1) * (1 - currValue/time)
    marker.setPosition points[index]

